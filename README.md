
# Frontend assignment 2: Translator app with React

## Table of contents

- Background
- Installation
- Usage
- Contributors


### Background

The goal of the project was to create a sign language translator as a Single Page Application using the React framework. The application has one main feature: to act as a “translator” from regular text to sign language. The application translates English words and short sentences to American sign language. For each letter there is a corresponding sign. There are three different views in the application. The usernames and original texts (to be translated) will be stored in https://er-noroff-api.herokuapp.com/translations.

1. Login page where users must provide a username
2. Translator page where user can provide the text and will get the sign language symbols as response
3. Profile page where user can browse all translation texts, delete texts and log out.

A wireframe defining the look of the pages and rough placement of components can be found [here](./wireframe/asl_translator_app_wireframe001.pdf)


### Installation

1. Clone this repository with HTTPS
2. Open project in IDE
3. Run app with command `npm run dev`

### Usage

When running, open [http://localhost:3000](http://localhost:3000) to view it in your browser.
The page will reload when you make changes.
You may also see any lint errors in the console.

A deployed version of the app can be found [here](https://calm-crag-47869.herokuapp.com/).

### Contributors

This exercise was created by Noroff School of Technology and this solution was implemented by Elmo Rohula and Lotta Lampola.