// lib & css imports
import React from 'react';
import './App.css';
import {
  BrowserRouter,
  Routes,
  Route
} from 'react-router-dom';

// page & component imports
import LoginPage  from './login/LoginPage';
import ProfilePage from './profile/ProfilePage';
import TranslationPage from './translation/TranslationPage';
import Navbar from './navbar/navbar';

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Navbar />
        <Routes>
          <Route path="/" element={ <LoginPage />}/>
          <Route path="/profile" element={ <ProfilePage />}/>
          <Route path="/translate" element={ <TranslationPage />}/>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
