import { createHeaders } from './index.js'

const apiUrl = process.env.REACT_APP_API_URL

const checkUserExists = async (username) => {
    try {
        const res = await fetch(`${apiUrl}?username=${username}`)
        if (!res.ok) {
            throw new Error('Couldn\'t complete request')
        }
        const data = await res.json();
        return [ null, data ];
    } catch (error) {
        return [ error.message, [] ];
    }
}

const createUser = async (username) => {
    try {
        const res = await fetch(apiUrl, {
            method: 'POST',
            headers: createHeaders(),
            body: JSON.stringify({
                username,
                translations: []
            })
        })
        if (!res.ok) {
            throw new Error('Couldn\'t create user ' + username)
        }
        const data = await res.json();
        return [ null, data ];
    } catch (error) {
        return [ error.message, [] ];
    }
}

export const loginUser = async (username) => {
    const [ checkError, user ] = await checkUserExists(username);
    if (checkError !== null) {
        return [ checkError, null ];
    }
    if (user.length > 0 ) {
        return [ null, user.pop() ];
    }
    return await createUser(username);
}

export const findUserById = async(userId) => {
    try {
        const response = await fetch(`${apiUrl}/${userId}`)
        if(!response.ok){
            throw new Error('Could not fetch user')
        }
        const user = await response.json()
        return [null,user]
    }
    catch (error){
        return [error.message,null]
    }
}