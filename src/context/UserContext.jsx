import React from 'react'
import { createContext, useContext, useState } from "react"; 
import { STORAGE_KEY_USER } from "../magicnumbers/storageKeys";
import { storageRead } from "../utils/storage";

const UserContext = createContext();

export const useUser = () => {
    return useContext(UserContext); // returns an object
}

const UserProvider = ({ children }) => {
    const [ user, setUser ] = useState( storageRead(STORAGE_KEY_USER) )

    const state = {
        user,
        setUser
    }
    
    return (
        <UserContext.Provider value={ state }>
            { children }
        </UserContext.Provider>
    )
}

export default UserProvider;