import React from 'react'
import { useState, useEffect } from 'react';
import { useForm } from 'react-hook-form'
import { loginUser } from '../api/user.js'
import { storageSave } from '../utils/storage.js';
import { useNavigate } from 'react-router-dom'
import { useUser } from '../context/UserContext.jsx';
import { STORAGE_KEY_USER } from "../magicnumbers/storageKeys";

const usernameConfig = {
    required: true,
    minLength: 3
}

const LoginForm = () => {
    const { register, handleSubmit, formState: { errors } } = useForm();
    const { user, setUser } = useUser();
    const navigate = useNavigate();

    const [ loading, setLoading] = useState(false);
    const [ apiError, setApiError] = useState(null);

    useEffect(() => {
        if (user !== null) {
            navigate('translate')
        }
    }, [ user, navigate ]);

    const onSubmit = async ({ username }) => {
        setLoading(true);

        const [ error, userResponse ] = await loginUser(username);
        if (error !== null) {
            setApiError(error);
        }
        if (userResponse !== null) {
            storageSave(STORAGE_KEY_USER, userResponse)
            setUser(userResponse);
        }
        setLoading(false)
    }

    const errorMsg = (() => {
        if (!errors.username) {
            return null;
        }
        if (errors.username.type === "required" ) {
            return <span>Username is required!</span>
        }
        if (errors.username.type === "minLength" ) {
            return (<span>Username must be at least { usernameConfig.minLength } characters long</span>)
        }
    })();

    return (
        <div className="loginDiv">
            <form onSubmit={ handleSubmit(onSubmit) }>
                <fieldset className="loginField">
                    <input className="loginInput" htmlFor="username" 
                        type="text" 
                        placeholder="What's your name?"
                        { ...register("username", usernameConfig) } />

                    { /* Act upon errors */}
                    { errorMsg }
                    <button className="profileBtn loginBtn" type="submit" disabled={ loading }>Login</button>
                </fieldset>
                { loading && <p className="loginAuxElement">Logging in...</p>}
                { apiError && <p className="loginAuxElement">{ apiError }</p>}
            </form>
        </div>
    )
}

export default LoginForm;