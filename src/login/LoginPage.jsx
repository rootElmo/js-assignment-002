import React from 'react';
import LoginForm from "./LoginForm";

const LoginPage = () => {
    return (
        <div className="login">
            <p style={{fontWeight: "bold"}}>Login page</p>
            <LoginForm />
        </div>
    );
}

export default LoginPage;