import { NavLink } from "react-router-dom"
import { useUser } from "../context/UserContext";

const Navbar = () => {

    const { user } = useUser();

    return (
        <nav>
            <div className="navContainer">
                { user !== null &&
                <>
                    <div className="navbarLinkContainer">
                        <NavLink to="/translate" className={"navbarLink"}>Translate</NavLink>
                    </div>
                    <div className="navbarLinkContainer">
                        <NavLink to="/profile" className={"navbarLink"}>User: {user.username}</NavLink>
                    </div>
                </>
                }
            </div>
        </nav>
    )
}

export default Navbar