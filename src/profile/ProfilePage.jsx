import React from 'react';
import withAuth from "../hoc/withAuth";
import ProfileTranslations from './ProfileTranslations'

const ProfilePage = () => {
    return (
        <>
        <div className='pageContent'>
            <p style={{fontWeight: "bold"}}>Profile page</p>
            <ProfileTranslations />
            </div>
        </>
    );
}

export default withAuth(ProfilePage);