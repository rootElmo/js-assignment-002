// import { useState, useEffect } from 'react';
import { useUser } from "../context/UserContext.jsx";
import { storageSave } from '../utils/storage.js';
import { STORAGE_KEY_USER } from '../magicnumbers/storageKeys';
import { useEffect } from "react";
import { findUserById } from "../api/user.js";
import { translationClearHistory } from "../api/translations.js";

const ProfileTranslations = () => {

    const { user, setUser } = useUser();
    const userTranslations = user.translations;

    //console.log(user.translations);

    useEffect(() => {
        const findUser = async () => {
            const [error, latestUser] = await findUserById(user.id)
            if(error === null) {
                storageSave(STORAGE_KEY_USER,latestUser)
                setUser(latestUser)
            }
        }
        //findUser()
    },[setUser,user.id])


    const handleLogOut = () => {
        if (window.confirm("Are you sure?")) {
            storageSave(STORAGE_KEY_USER, null);
            setUser(null);
        }
    }

    const handleClearHistory = async() => {
        if (!window.confirm('Are you sure you want to clear history?')){
            return
        }
        const [clearError] = await translationClearHistory(user.id)

        if(clearError !== null) {
            return
        }

        const updateUser = {
            ...user,
            translations: []
        }
        storageSave(STORAGE_KEY_USER, updateUser);
        setUser(updateUser)
    }

    // In the future, take the mapping logic out of the
    // return statement
    return (
        <div className="translationDiv">
            <p className="translationListTitle">Translations</p>
            <ul className="translationUl">
                { userTranslations.map(translation => <li className="translationLi" key={translation}>{ translation }</li>) }
            </ul>
            <div className="profileBtnContainer">
                <button className="profileBtn clearBtn" onClick={handleClearHistory}>Clear translations</button>
                <button className="profileBtn logoutBtn" onClick={ handleLogOut }>Logout</button>
            </div>
        </div>
    );
}

export default ProfileTranslations