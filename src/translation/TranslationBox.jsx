import React from "react";

const TranslationBox = ({user,pictures}) => {

    

    return(
        <div className="translationBoxBorders">
        <p className="translationBox">
            {pictures.map((p,index) => 
            <img className = "translationImages" src={p} alt={p} key={index}/>
            )}
        </p>
        </div>
    );
}

export default TranslationBox;