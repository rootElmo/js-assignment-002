import React, { useState } from "react";
import { useUser } from "../context/UserContext.jsx";
import { storageSave } from '../utils/storage.js';
import { STORAGE_KEY_USER } from '../magicnumbers/storageKeys';
import TranslationBox from "./TranslationBox";
import { translationAdd } from "../api/translations.js";

const TranslationForm = () => {
    const { user, setUser } = useUser();
    const [ text, setText ] = useState('');
    const [isShown, setIsShown] = useState(false);
    const [pictures, setPictures] = useState([]);

    const handleTextChange = (event) => {
        setText(event.target.value);
    };

    const handleTranslateSubmit = event => {
        event.preventDefault();
        setIsShown(true);
        setPictures(translate(text));
        addTranslationToUser(text);
        event.target.reset();
    };

    const addTranslationToUser = async (text) => {
        const [error, updatedUser] = await translationAdd(user,text)
        if(error !== null){
            return
        }
        //to keep ui and server state in sync
        storageSave(STORAGE_KEY_USER,updatedUser)
        //update context state
        setUser(updatedUser);
    }

    const translate = text => {
        text = text.toLowerCase();
        let temp = [];
        for (let i=0; i < text.length; i++){
            if (text.charAt(i).match(/[a-z]/i)){
            let alphabet = text.charAt(i);
            let newPicture = getPicture(alphabet);
            temp.push(newPicture);
            }
        }
        return temp;
      
    }

    const getPicture = alphabet =>{
        return '/individial_signs/'+alphabet+'.png'
    }

  

    return (
        <>
        <div className="translation">
        <form onSubmit={ handleTranslateSubmit } className='translationForm'>
            <fieldset>
                <div className="inputAndButton">
                <input type="text" value={ text.value } onChange={ handleTextChange } placeholder='To be translated'/>
                <button type="submit" className="translateButton">Translate</button>
                </div>
                </fieldset>
        </form>
        {isShown && <TranslationBox user={user} pictures={pictures}/>} 
        </div>
        </>
    );
}

export default TranslationForm

