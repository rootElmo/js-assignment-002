import React, { useState } from 'react';
import TranslationForm from './TranslationForm';
import withAuth from "../hoc/withAuth";

const TranslationPage = () => {



    return (
        <>
        <div className='pageContent'>
            <TranslationForm/>
            </div>
        </>
    );
}

export default withAuth(TranslationPage);