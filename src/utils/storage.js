import { STORAGE_KEY_USER } from "../magicnumbers/storageKeys"

export const storageSave = (key, value) => {
    if (!key && typeof key !== 'string'){
        throw new Error('StorageSave: No storage key provided')
    }
    if(!value && key !== STORAGE_KEY_USER){
        throw new Error('StorageSave: No value provided for key'+key)
    }
    localStorage.setItem(key, JSON.stringify(value))
}

export const storageRead = (key) => {
    const data = localStorage.getItem(key);
    if (data !== null) {
        return JSON.parse(data);
    }
    return null;
}